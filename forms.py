from flask.ext.wtf import Form
from wtforms.fields import TextField, TextAreaField, SelectField
from wtforms import SubmitField, validators, ValidationError, PasswordField
from models import db, wallet, billers, admin

class SignupForm(Form):
    username = TextField("username", [validators.Required("Username")])
    #lastname = TextField("Last name", [validators.Required("Last name")])
    password = PasswordField('Password', [validators.Required("Please enter a password.")])
    email = TextField("Email", [validators.Required("Email")])

    #company = TextField("company")
    submit = SubmitField("Create account")


    def __init__(self, *args, **kwargs):
        Form.__init__(self, *args, **kwargs)

    def validate(self):
        if not Form.validate(self):
            return False

        user = billers.query.filter_by(email=self.email.data.lower()).first()
        if user:
            self.email.errors.append("This email is already taken")
            return False
        else:
            return True

class SigninForm(Form):
    email = TextField("Email",  [validators.Required("Please enter your email address."), validators.Email("Please enter your email address.")])
    password = PasswordField('Password', [validators.Required("Please enter a password.")])
    submit = SubmitField("Sign In")

    def __init__(self, *args, **kwargs):
        Form.__init__(self, *args, **kwargs)

    def validate(self):
        if not Form.validate(self):
            return False

        user = wallet.query.filter_by(email=self.email.data.lower()).first()
        if user and user.check_password(self.password.data):
            return True
        else:
            self.email.errors.append("Invalid e-mail or password")
            return False

class addmoneyForm(Form):
    addmoney = TextField("money", [validators.Required("money")])
    submit = SubmitField("submit")

    def __init__(self, *args, **kwargs):
        Form.__init__(self, *args, **kwargs)

    def validate(self):
        if not Form.validate(self):
            return False
        else:
            return True

class editprofileForm(Form):
    update_username = TextField("username1", [validators.Required("username1")])
    update_password = PasswordField('Password1', [validators.Required("Please enter a password.")])
    submit = SubmitField("Update details")

    def __init__(self, *args, **kwargs):
        Form.__init__(self, *args, **kwargs)

    def validate(self):
        if not Form.validate(self):
            return False
        else:
            return True

class unloadmoneyForm(Form):
    unloadmoney = TextField("money", [validators.Required("money")])
    submit = SubmitField("submit")

    def __init__(self, *args, **kwargs):
        Form.__init__(self, *args, **kwargs)

    def validate(self):
        if not Form.validate(self):
            return False
        else:
            return True


class billers_SignupForm(Form):
    username = TextField("username", [validators.Required("Username")])
    #lastname = TextField("Last name", [validators.Required("Last name")])
    password = PasswordField('Password', [validators.Required("Please enter a password.")])
    email = TextField("Email", [validators.Required("Email")])
    category = SelectField('category', choices=[("Mobile recharge", 'Mobile recharge'), ("DTH recharge", 'DTH recharge'), ("Electric Bill payment", 'Electric Bill payment')])
    commission = TextField('commission', [validators.Required("Please enter Commission.")])

    #company = TextField("company")
    submit = SubmitField("Create account")


    def __init__(self, *args, **kwargs):
        Form.__init__(self, *args, **kwargs)

    def validate(self):
        if not Form.validate(self):
            return False

        user = billers.query.filter_by(email=self.email.data.lower()).first()
        if user:
            self.email.errors.append("This email is already taken")
            return False
        else:
            return True


class billers_SigninForm(Form):
    email = TextField("Email",  [validators.Required("Please enter your email address."), validators.Email("Please enter your email address.")])
    password = PasswordField('Password', [validators.Required("Please enter a password.")])
    submit = SubmitField("Sign In")

    def __init__(self, *args, **kwargs):
        Form.__init__(self, *args, **kwargs)

    def validate(self):
        if not Form.validate(self):
            return False

        user = billers.query.filter_by(email=self.email.data.lower()).first()
        if user and user.check_password(self.password.data):
            return True
        else:
            self.email.errors.append("Invalid e-mail or password")
            return False


class paybillForm(Form):
    paymoney = TextField("money", [validators.Required("money")])
    username = TextField("username", [validators.Required("username")])
    submit = SubmitField("Transfer")

    def __init__(self, *args, **kwargs):
        Form.__init__(self, *args, **kwargs)

    def validate(self):
        if not Form.validate(self):
            return False
        else:
            return True


class adminsignForm(Form):
    email = TextField("Email",  [validators.Required("Please enter your email address."), validators.Email("Please enter your email address.")])
    password = PasswordField('Password', [validators.Required("Please enter a password.")])
    submit = SubmitField("Sign In")

    def __init__(self, *args, **kwargs):
        Form.__init__(self, *args, **kwargs)

    def validate(self):
        if not Form.validate(self):
            return False

        user = admin.query.filter_by(email=self.email.data.lower()).first()
        if user and user.check_password(self.password.data):
            return True
        else:
            self.email.errors.append("Invalid e-mail or password")
            return False
