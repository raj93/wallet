
from flask import Flask
from routes import blueprint1
from models import db

app = Flask(__name__)
app.config['SECRET_KEY']='not a secret'
app.secret_key = 'development key'



app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:@127.0.0.1/wallet'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
app.config['SQLALCHEMY_ECHO'] = True

db.init_app(app)
app.register_blueprint(blueprint1)
