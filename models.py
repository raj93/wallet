
from flask.ext.sqlalchemy import SQLAlchemy
#from sqlalchemy import Integer
from werkzeug import generate_password_hash, check_password_hash

db = SQLAlchemy()


class wallet(db.Model):
    __tablename__ = 'user'
    id = db.Column(db.Integer, primary_key = True)
    username = db.Column(db.String(100))
    password = db.Column(db.String(54))
    email = db.Column(db.String(120), unique=True)
    money = db.Column(db.Integer, nullable = True)

    def __init__(self, username, password, email, money = 0):
      self.username = username.title()
      self.set_password(password)
      self.email = email.lower()
      self.money = money

    def set_password(self, password):
      self.password = password

    def check_password(self, password):
        return self.password == password


class billers(db.Model):
    __tablename__ = 'billers'
    id = db.Column(db.Integer, primary_key = True)
    username = db.Column(db.String(100))
    password = db.Column(db.String(54))
    email = db.Column(db.String(120), unique=True)
    money = db.Column(db.Integer, nullable = True)
    category = db.Column(db.String(100), nullable = False)
    commission = db.Column(db.Integer, nullable = True)

    def __init__(self, username, password, email, category, commission=1, money = 0):
      self.username = username.title()
      self.set_password(password)
      self.email = email.lower()
      self.money = money
      self.category = category
      self.commission = commission

    def set_password(self, password):
      self.password = password

    def check_password(self, password):
        return self.password == password

class admin(db.Model):
    __tablename__ = 'admin'
    id = db.Column(db.Integer, primary_key = True)
    username = db.Column(db.String(100))
    password = db.Column(db.String(54))
    email = db.Column(db.String(120), unique=True)
    money = db.Column(db.Integer, nullable = True)

    def __init__(self, username, password, email, money = 0):
      self.username = username.title()
      self.set_password(password)
      self.email = email.lower()
      self.money = money

    def set_password(self, password):
      self.password = password

    def check_password(self, password):
        return self.password == password