
from flask import Flask, render_template, request, flash, session, redirect, url_for, Blueprint

from forms import SignupForm, SigninForm, addmoneyForm, editprofileForm, unloadmoneyForm, billers_SigninForm, billers_SignupForm, paybillForm, adminsignForm

#from flask.ext.mail import Message, Mail

from models import db, wallet, billers, admin

blueprint1= Blueprint('simple_page', __name__,
                        template_folder='templates')


@blueprint1.route('/', methods = ['GET', 'POST'])
def index():
    if request.method == 'POST':
        return render_template('index.html')
    else:
        return render_template('index.html')


@blueprint1.route('/signup', methods = ['GET', 'POST'])
def signup():
    form = SignupForm()

    #if 'email' in session:
        #return redirect(url_for('simple_page.profile'))

    if request.method == 'POST':
        if form.validate() == False:
            return render_template('signup.html', form = form)
        else:
            newuser = wallet(form.username.data, form.password.data, form.email.data ) #form.company.data
            db.session.add(newuser)
            db.session.commit()

            session['email'] = newuser.email

            return redirect(url_for('simple_page.profile'))

    elif request.method == 'GET':
        return render_template('signup.html', form = form)


@blueprint1.route('/profile')
def profile():
    print("hello")
    if 'email' not in session:
        return redirect(url_for('simple_page.signin'))

    user = wallet.query.filter_by(email=session['email']).first()

    if user is None:
        return redirect(url_for('simple_page.profile'))
    else:
        return render_template('profile.html', username = user.username, addmoney = user.money)


@blueprint1.route('/signin', methods=['GET', 'POST'])
def signin():
    form = SigninForm()

    if 'email' in session:
        return redirect(url_for('simple_page.profile'))

    if request.method == 'POST':
        if form.validate() == False:
            return render_template('signin.html', form=form)
        else:
            session['email'] = form.email.data
            return redirect(url_for('simple_page.profile'))

    elif request.method == 'GET':
        return render_template('signin.html', form=form)

@blueprint1.route('/signout')
def signout():

    if 'email' not in session:
        return redirect(url_for('simple_page.index'))

    session.pop('email', None)
    return redirect(url_for('simple_page.index'))


@blueprint1.route('/addmoney', methods=['GET', 'POST'])
def addmoney():
    form = addmoneyForm()

    if request.method == 'POST':
        if form.validate() == False:
            #print 'false'
            return render_template('addmoney.html', form=form)
        else:
            user = wallet.query.filter_by(email=session['email']).first()
            user.money += int(form.addmoney.data)
            print user.money
            db.session.add(user)
            db.session.commit()
        return render_template('addmoney.html', form=form, addmoney = user.money)
    return render_template('addmoney.html', form=form)


@blueprint1.route('/editprofile', methods=['GET', 'POST'])
def editprofile():
    form = editprofileForm()

    if request.method == 'POST':
        if form.validate() == False:
            return render_template('edit_profile.html', form=form)
        else:
            user = wallet.query.filter_by(email=session['email']).first()
            user.username = form.update_username.data
            user.password = form.update_password.data
            db.session.add(user)
            db.session.commit()
        return render_template('edit_profile.html', form = form, update_username = user.username, update_password = user.password)
    return render_template('edit_profile.html', form=form)


@blueprint1.route('/unloadmoney', methods=['GET', 'POST'])
def unloadmoney():
    form = unloadmoneyForm()

    if request.method == 'POST':
        if form.validate() == False:
            #print 'false'
            return render_template('unloadmoney.html', form=form)
        else:
            user = billers.query.filter_by(email=session['email']).first()
            user.money -= int(form.unloadmoney.data)
            print user.money
            db.session.add(user)
            db.session.commit()
        return render_template('unloadmoney.html', form=form, unloadmoney = user.money)
    return render_template('unloadmoney.html', form=form)


@blueprint1.route('/billers_signup', methods=['GET', 'POST'])
def billers_signup():
    form = billers_SignupForm()

    #if 'email' in session:
        #return redirect(url_for('simple_page.profile'))

    if request.method == 'POST':
        if form.validate() == False:
            return render_template('billers_signup.html', form = form)
        else:
            newuser = billers(form.username.data, form.password.data, form.email.data, form.category.data, form.commission.data ) #form.company.data
            db.session.add(newuser)
            db.session.commit()

            #session['email'] = newuser.email

            return redirect(url_for('simple_page.admin_profile'))

    elif request.method == 'GET':
        return render_template('billers_signup.html', form = form)


@blueprint1.route('/billers_signin', methods=['GET', 'POST'])
def billers_signin():
    form = billers_SigninForm()

    if 'email' in session:
        return redirect(url_for('simple_page.billers_profile'))

    if request.method == 'POST':
        if form.validate() == False:
            return render_template('billers_signin.html', form=form)
        else:
            session['email'] = form.email.data
            return redirect(url_for('simple_page.billers_profile'))

    elif request.method == 'GET':
        return render_template('billers_signin.html', form=form)


@blueprint1.route('/billersprofile')
def billers_profile():
    print("hello")
    if 'email' not in session:
        return redirect(url_for('simple_page.billers_profile'))

    user = billers.query.filter_by(email=session['email']).first()

    if user is None:
        return redirect(url_for('simple_page.billers_profile'))
    else:
        return render_template('billers_profile.html', username = user.username)


@blueprint1.route('/paybill', methods=['GET', 'POST'])
def paybill():
    form = paybillForm()

    if request.method == 'POST':
        if form.validate() == False:
            return render_template('paybill.html', form=form)
        else:
            user = wallet.query.filter_by(email=session['email']).first()
            user.money -= int(form.paymoney.data)
            print user.money
            db.session.add(user)
            db.session.commit()

            user1 = billers.query.filter_by(username=form.username.data).first()
            com = int(form.paymoney.data)*(int(user1.commission)/100.0)
            user1.money += int(form.paymoney.data)- int(com)
            db.session.add(user1)
            db.session.commit()

            user2 = admin.query.filter_by(username = "admin").first()
            user2.money += int(com)
            db.session.add(user2)
            db.session.commit()

        return render_template('paybill.html', form=form, paymoney = user.money, username = user.username)
    return render_template('paybill.html', form=form)


@blueprint1.route('/admin_signin', methods=['GET', 'POST'])
def admin_signin():
    form = adminsignForm()

    if 'email' in session:
        return redirect(url_for('simple_page.admin_profile'))

    if request.method == 'POST':
        if form.validate() == False:
            return render_template('admin_signin.html', form=form)
        else:
            session['email'] = form.email.data
            return redirect(url_for('simple_page.admin_profile'))

    elif request.method == 'GET':
        return render_template('admin_signin.html', form=form)


@blueprint1.route('/adminprofile')
def admin_profile():
    print("hello")
    if 'email' not in session:
        return redirect(url_for('simple_page.admin_signin'))

    user = admin.query.filter_by(email=session['email']).first()

    if user is None:
        return redirect(url_for('simple_page.admin_profile'))
    else:
        return render_template('admin_profile.html', username = user.username)
