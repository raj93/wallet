"""empty message

Revision ID: 3b51e8b3a81f
Revises: fd8b19dfdae
Create Date: 2015-08-10 13:25:03.464829

"""

# revision identifiers, used by Alembic.
revision = '3b51e8b3a81f'
down_revision = 'fd8b19dfdae'

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql

def upgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.add_column('billers', sa.Column('category', sa.String(length=100), nullable=False))
    op.add_column('billers', sa.Column('commission', sa.Integer(), nullable=True))
    op.drop_column('user', 'dummmy')
    ### end Alembic commands ###


def downgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.add_column('user', sa.Column('dummmy', mysql.INTEGER(display_width=11), autoincrement=False, nullable=True))
    op.drop_column('billers', 'commission')
    op.drop_column('billers', 'category')
    ### end Alembic commands ###
